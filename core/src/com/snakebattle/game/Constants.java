package com.snakebattle.game;

public class Constants {
    // Visible game world is 5 meters wide
    public static final float VIEWPORT_WIDTH = 10.0f;
    // Visible game world is 5 meters tall
    public static final float VIEWPORT_HEIGHT = 10.0f;   public static final int FILD_SIZE = 8;
    public static final int FURI_LENGTH = 10;
    //BONUSES
    public static final int APPLE_BONUS = 1;
    public static final int COIN_BONUS = 1;
    public static final int STONE_BONUS = 3;
    public static final int SNAKE_QUANTITY = 5;
    //assets
    public static final String TEXTURE_ATLAS_OBJECTS =
            "android/assets/images/snakefield.atlas";
    // Location of image file for level 01
    public static final String LEVEL_01 = "android/assets/levels/level-01.png";
    //mimimax
    public static final int EPIC_BIG_VALUE = 500;
    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 255;
    public static final int NOT_INITIALIZED = 255;
    public static final int EMPTY = 0;
    public static final int RABBIT = 1;
    public static final int WOLF = 255;
}
