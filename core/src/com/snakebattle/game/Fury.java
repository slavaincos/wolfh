package com.snakebattle.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Fury extends AbstractGameObject {
    private TextureRegion regFury;
    public boolean collected;
    public Fury () {
        init();
    }
    private void init () {
        dimension.set(0.5f, 0.5f);
        regFury = Assets.instance.fury.fury;
// Set bounding box for collision detection
        bounds.set(0, 0, dimension.x, dimension.y);
        collected = false;
    }

    public void render (SpriteBatch batch) {
        if (collected) return;
        TextureRegion reg = null;
        reg = regFury;
        batch.draw(reg.getTexture(),
                position.x, position.y,
                origin.x, origin.y,
                dimension.x, dimension.y,
                scale.x, scale.y,
                rotation,
                reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(),
                false, false);
    }
}
