package com.snakebattle.game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class MiniMax {
    public static final MiniMax instance = new MiniMax();
    public Vector2[] possibleMoves = {
                                    new Vector2(-1, -1),    //left, down
                                    new Vector2(1, -1),    //right, down
                                    new Vector2(-1,1),      //left, up
                                    new Vector2(1,1)        //right, up
    };
    public int map[][] = new int[8][8];
    public Array<Vector2> seachWay = new Array<Vector2>();
    public Level level;

    private MiniMax() {
    }

    public boolean isGameOver(AbstractGameObject winner){
        boolean canMove = false;
        Apple rabbit = level.apples.get(0);

        winner = null;
        for (GoldCoin wolf: level.goldcoins){
            for (int i = 0; i < 2; i++){
                if (canMoveToPosition(wolf, possibleMoves[i]))
                    canMove = true;
            }
        }

        if (!canMove && rabbit.position.y == 8)
            winner = rabbit;

        canMove = false;
        for (int i = 0; i < 4; i++)
            if(canMoveToPosition(rabbit, possibleMoves[i]))
                canMove = true;

        if(!canMove) winner = level.goldcoins.get(0);
        if(winner != null) return true;

        return false;
    }

    public void  prepareMap(){
        for (int i = 0; i < Constants.FILD_SIZE; i++)
            for(int j = 0; j < Constants.FILD_SIZE; j++)
                map[i][j] = 0;
        Apple rabbit = level.apples.get(0);
        map[(int)rabbit.position.y - 1][(int) rabbit.position.x - 1] = Constants.RABBIT;
        for (GoldCoin wolf: level.goldcoins)
            map[(int) wolf.position.y - 1][(int) wolf.position.x - 1] = Constants.WOLF;

    }

    public Vector2 dequeue(Array<Vector2> array){
        array.reverse();
        Vector2 res = array.pop();
        array.reverse();
        return  res;
    }
    private boolean positionNotFound(Vector2 position){
        for (Vector2 vector: seachWay)
            if(vector.x == position.x && vector.y == position.y){
                return false;
            }
        return true;
    }
    public int getHeuristicEvaluation(){
        Apple rabbit = level.apples.get(0);
        if (rabbit.position.y == 8) return 0;
        seachWay.clear();
        seachWay.add(rabbit.position);
        while (seachWay.notEmpty()){
            Vector2 currentPosition = dequeue(seachWay); //seachWay.pop(); //
            for (int i = 0; i < 4; i++)
                if(canMove(new Vector2(currentPosition.x + possibleMoves[i].x, currentPosition.y + possibleMoves[i].y))){
                    Vector2 newPosition = new Vector2(currentPosition.x + possibleMoves[i].x, currentPosition.y + possibleMoves[i].y);
                    if (map[(int)newPosition.y - 1][(int)newPosition.x - 1] == 0){
                    //if (positionNotFound(newPosition)){
                        seachWay.add(newPosition);
                        map[(int) newPosition.y - 1][(int)newPosition.x - 1] = map[(int) currentPosition.y - 1][(int) currentPosition.x - 1] + 1;
                    }
                }
        }
        //proximity&gaps
        int maxRow = 0;
        int minRow = 7;
        int maxColumn = 0;
        int minColumn = 7;
        Array<Integer> columns = new Array<Integer>();
        Array<Integer> rows = new Array<Integer>();

        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                if (map[i][j] ==  255) {
                    columns.add(j);
                    rows.add(i);
                    minRow = Math.min(minRow, i);
                    maxRow = Math.max(maxRow, i);
                    minColumn = Math.min(minColumn, j);
                    maxColumn = Math.max(maxColumn, j);
                }
        int startC = columns.get(0);
        int startR = rows.get(0);
        int colGaps = 0;
        int rowGaps = 0;
        int gapDelta = 0;

        for(int i = 1; i < 4; i++) {
            rowGaps = Math.abs(rows.get(i) - rows.get(i-1));
            colGaps = Math.abs(columns.get(i) - columns.get(i-1));
            switch (rowGaps) {
                case 0: gapDelta = colGaps/2 - 1;
                case 1: gapDelta = colGaps/2;
                case 2: gapDelta = colGaps/2;
                case 3: gapDelta = columns.get(i-1) == 0 && colGaps == 1? 0: (colGaps + 1)/2;
            }
        }

         int min = Constants.MAX_VALUE;
        for (int i = 0; i < 4; i++)
 //           if(map[7][i *2] > Constants.MIN_VALUE && map[7][i *2] > min)
              if(map[7][i *2] < min && map[7][i *2] > 0)
                    min = map[7][i *2];
            return min - 1 - (maxRow - minRow) + (maxColumn - minColumn) + gapDelta;
    }

    public void temporaryMonsterMovement(int monsterIndex, int x, int y){
        Apple rabbit = level.apples.get(0);
        GoldCoin wolf = monsterIndex == 0? null: level.goldcoins.get(monsterIndex - 1);
        if (monsterIndex == 0) {
            map[(int)rabbit.position.y - 1][(int)rabbit.position.x - 1] = Constants.EMPTY;
            map[(int)rabbit.position.y + y - 1][(int)rabbit.position.x + x - 1] = Constants.RABBIT;
            rabbit.position.add(x, y);
        }
        else {
            map[(int)wolf.position.y - 1][(int)wolf.position.x - 1] = Constants.EMPTY;
            map[(int)wolf.position.y + y - 1][(int)wolf.position.x + x - 1] = Constants.WOLF;
            wolf.position.add(x, y);
        }
    }

    public int runMinMax(AbstractGameObject monster, int recursiveLevel, int alpha, int betta){

        int heuristic = -1;
        if(recursiveLevel == 0) prepareMap();
        int test = Constants.NOT_INITIALIZED;

        //At last tree level (leaf) return heuristic value
        if(recursiveLevel >= 2){
            heuristic = getHeuristicEvaluation();
            prepareMap();
            return heuristic;
        }
        int bestMove = Constants.NOT_INITIALIZED;
        boolean isWolf = monster.getClass().getName().equals("com.snakebattle.game.GoldCoin");
        int MinMax = isWolf ? Constants.MIN_VALUE: Constants.MAX_VALUE;
        for(int i = (isWolf? 0: 8); i < (isWolf? 8: 12); i++){
            int currentMonster = isWolf? i / 2 + 1: 0;
            Vector2 curMonsterPos = currentMonster == 0? level.apples.get(0).position: level.goldcoins.get(currentMonster - 1).position;
            Vector2 curMove = possibleMoves[isWolf? i % 2: 3 - i % 4];
            if(canMove((int)(curMonsterPos.x + curMove.x), (int)(curMonsterPos.y + curMove.y))){
                temporaryMonsterMovement(currentMonster, (int) curMove.x, (int) curMove.y);
                //move quality testing
                test = runMinMax(currentMonster == 0? level.goldcoins.get(0): level.apples.get(0), recursiveLevel + 1, alpha, betta);
                //restore move
                temporaryMonsterMovement(currentMonster, -(int)curMove.x, -(int) curMove.y);
                //keep move if it's best
                if((test > MinMax && isWolf) || (test < MinMax && !isWolf) || bestMove == Constants.NOT_INITIALIZED){
                    MinMax = test;
                    bestMove = i;
                }
/*
                else if (test == MinMax && isWolf)
                    if(((int)(curMonsterPos.x + curMove.x) - (int)(level.apples.get(0).position.x) == 1) &&
                            ((int)(curMonsterPos.y + curMove.y) - (int)(level.apples.get(0).position.y) == 1)){
                        bestMove = i;
                }
*/
                if(isWolf) alpha = Math.max(alpha, test);
                else betta = Math.min(betta, test);
                if (betta < alpha) break;
            }
        }
        //for terminals when there in no move return heuristic function
        if(bestMove == Constants.NOT_INITIALIZED){
            heuristic = getHeuristicEvaluation();
            prepareMap();
            return heuristic;
        }
        //movement if it's best
        if(recursiveLevel == 0 && bestMove != Constants.NOT_INITIALIZED){
            if (isWolf)
                level.goldcoins.get(bestMove / 2).position.add(possibleMoves[bestMove % 2]);
            else level.apples.get(0).position.add(possibleMoves[3 - bestMove % 4]);
        }
        return MinMax;
    }

    public boolean checkRange(float x, float y){
        return (x > 0 && y > 0 && x <= 8 && y <=8);

    }

    public boolean checkRange(Vector2 point){
        return checkRange(point.x, point.y);
    }

    public boolean canMove(Vector2 v){
        return canMove((int)v.x, (int) v.y);
    }

    public boolean canMove(int x, int y){
        if(!checkRange(x, y))
            return false;
        for (GoldCoin wolf: level.goldcoins ){
            if(wolf.position.x == x && wolf.position.y == y)
                return false;
         Apple rabbit = level.apples.get(0);
         if (rabbit.position.x == x && rabbit.position.y == y)
             return false;
        }
        return true;
    }

    boolean canMoveToPosition(AbstractGameObject obj, Vector2 pos){

        if(!checkRange(pos)) return false;
        if ((pos.x + pos.y) % 2 == 0) return false;
        Vector2 oldPosition = new Vector2(obj.position);
        Vector2 diff = oldPosition.sub(pos);
        if (Math.abs(diff.x) != 1 || Math.abs(diff.y) != 1)
            return false;
        if (oldPosition == pos) return false;

        for (GoldCoin wolf: level.goldcoins ) {
            if(wolf.position.x == pos.x && wolf.position.y == pos.y) return false;
        }
        if (oldPosition.y > pos.y && (GoldCoin)obj == null) return false;
            return true;
    }
}
