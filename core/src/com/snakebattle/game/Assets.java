package com.snakebattle.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;

public class Assets implements Disposable, AssetErrorListener {
    public static final String TAG = Assets.class.getName();
    public static final Assets instance = new Assets();
    private AssetManager assetManager;
    public AssetRock rock;
    public AssetGoldCoin goldCoin;
    public AssetStone stone;
    public AssetApple apple;
    public AssetFury fury;
    public AssetSnake snake;

    // singleton: prevent instantiation from other classes
    private Assets() {
    }

    public class AssetRock {
        public final TextureAtlas.AtlasRegion edge;
        public final TextureAtlas.AtlasRegion middle;
        public final TextureAtlas.AtlasRegion side;

        public AssetRock (TextureAtlas atlas) {
            edge = atlas.findRegion("white");
            middle = atlas.findRegion("black");
            side = atlas.findRegion("rock_side_middle");
        }
    }

    public class AssetSnake {
        public final TextureAtlas.AtlasRegion head;
        public final TextureAtlas.AtlasRegion body;
        public final TextureAtlas.AtlasRegion tale;
        public final TextureAtlas.AtlasRegion wave;
        public final TextureAtlas.AtlasRegion head_rotated;
        public final TextureAtlas.AtlasRegion body_rotated;
        public final TextureAtlas.AtlasRegion tale_rotated;
        public final TextureAtlas.AtlasRegion wave_rotated;

        public AssetSnake (TextureAtlas atlas) {
            head = atlas.findRegion("snake_head");
            body = atlas.findRegion("snake_body");
            tale = atlas.findRegion("snake_tale");
            wave = atlas.findRegion("snake_wave");
            head_rotated = atlas.findRegion("snake_head-90");
            body_rotated = atlas.findRegion("snake_body-90");
            tale_rotated = atlas.findRegion("snake_tale-90");
            wave_rotated = atlas.findRegion("snake_wave-90");
        }
    }

    public class AssetGoldCoin {
        public final TextureAtlas.AtlasRegion goldCoin;
        //public final Animation animGoldCoin;

        public AssetGoldCoin(TextureAtlas atlas) {
            goldCoin = atlas.findRegion("W_Checker");

            // Animation: Gold Coin
        }
    }

    public class AssetStone {
        public final TextureAtlas.AtlasRegion stone;
        //public final Animation animGoldCoin;

        public AssetStone(TextureAtlas atlas) {
            stone = atlas.findRegion("item_stone");

        }
    }

        public class AssetApple {
            public final TextureAtlas.AtlasRegion apple;
            //public final Animation animGoldCoin;

            public AssetApple(TextureAtlas atlas) {
                apple = atlas.findRegion("B_Checker");

            }
        }

    public class AssetFury {
        public final TextureAtlas.AtlasRegion fury;
        //public final Animation animGoldCoin;

        public AssetFury(TextureAtlas atlas) {
            fury = atlas.findRegion("item_pill");

        }
    }

    public void init(AssetManager assetManager) {
        this.assetManager = assetManager;
// set asset manager error handler
        assetManager.setErrorListener(this);
// load texture atlas
        assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS,
                TextureAtlas.class);
// start loading assets and wait until finished
        assetManager.finishLoading();
        Gdx.app.debug(TAG, "# of assets loaded: "
                + assetManager.getAssetNames().size);
        for (String a : assetManager.getAssetNames())
            Gdx.app.debug(TAG, "asset: " + a);
        //My addition
        TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);
// enable texture filtering for pixel smoothing
        for (Texture t : atlas.getTextures())
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
// create game resource objects
        rock = new AssetRock(atlas);
        goldCoin = new AssetGoldCoin(atlas);
        stone = new AssetStone(atlas);
        apple = new AssetApple(atlas);
        fury = new AssetFury(atlas);
        snake = new AssetSnake(atlas);
    }

    @Override
    public void dispose () {
        assetManager.dispose();
    }

    public void error (AssetDescriptor assetDescriptor,
                       Throwable throwable) {
        Gdx.app.error(TAG, "Couldn't load asset '"
                + assetDescriptor.fileName + "'", (Exception)throwable);
    }
}
