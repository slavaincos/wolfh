package com.snakebattle.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Snake extends AbstractGameObject {
    public enum DIRECTION {
        NORTH,
        WEST,
        EAST,
        SOUTH;
    };
    public static final String TAG = Snake.class.getName();

    private TextureRegion regHead;
    private TextureRegion regBody;
    private TextureRegion regWave;
    private TextureRegion regTale;
    public int score;
    private int rage;
    public boolean dead;
    public boolean flagApple;
    private Array<Connection> con;
    public int length;
    private int currentKey;

    public Array<Part> snakeParts;
    private WorldController mWorldController;

    public Snake(Array<Vector2> parts, WorldController wc) {
        init(parts, wc);
    }

    public class Part{
        public Vector2 position;
        public DIRECTION direction;
        public Vector2 dimension;
        public Rectangle bounds;
        public TextureRegion reg;
       // public Vector2 renderPos;

        Part(DIRECTION dir, Vector2 pos, TextureRegion regPart){
            direction = dir;
            position = pos;
            dimension = new Vector2(1f, 1f);
            bounds = new Rectangle();
            bounds.set(0, 0, dimension.x, dimension.y);
            reg = regPart;
            //renderPos = new Vector2(pos);
        }
    }

    private class Connection{
        private int key;
        private DIRECTION from;
        private DIRECTION to;
        public Connection(int k, DIRECTION f, DIRECTION t){
            key = k;
            from = f;
            to = t;
        }
    }

    public boolean isRaged(){
        return rage > 0;
    }

    private void partLoading(DIRECTION dir){
        if(dir == DIRECTION.NORTH || dir == DIRECTION.SOUTH) {
            regHead = Assets.instance.snake.head;
            regBody = Assets.instance.snake.body;
            regTale = Assets.instance.snake.tale;
            regWave = Assets.instance.snake.wave;
        }
        else {
            regHead = Assets.instance.snake.head_rotated;
            regBody = Assets.instance.snake.body;
            regTale = Assets.instance.snake.tale_rotated;
            regWave = Assets.instance.snake.wave_rotated;
        }

    }
    private void init(Array<Vector2> parts, WorldController wc) {
        mWorldController = wc;
        partLoading(DIRECTION.NORTH);
        length = 2;
        rage = 0;
        score = 0;
        dead = false;
        flagApple = false;
        snakeParts = new Array<Part>();
        for (int i = 0; i < parts.size; i++)
            snakeParts.add(new Part(DIRECTION.NORTH, parts.get(i), i == 0? regHead: regTale));
 // Set bounding box for collision detection
        bounds.set(0, 0, dimension.x, dimension.y);
        con = new Array<>();
    }

    public void moveTo(DIRECTION dir){
     //checking apple collision
        Part head = snakeParts.get(0);
        Rectangle r = new Rectangle(head.position.x,
                head.position.y,
                head.bounds.width,
                head.bounds.height);
        switch (dir) { //head move to dir
            case SOUTH:
                r.y = r.y - 1;
                break;
            case WEST:
                r.x = r.x - 1;
                break;
            case EAST:
                r.x = r.x + 1;
                break;
            case NORTH:
                r.y = r.y + 1;
        }

        mWorldController.appleCollision(r, this);
        if (flagApple){
            addPart(dir);
            return;
        }
        Part headpart = snakeParts.get(0);
//moving back forbidden
        if ((headpart.direction == DIRECTION.EAST && dir == DIRECTION.WEST) ||
                (headpart.direction == DIRECTION.NORTH && dir == DIRECTION.SOUTH) ||
                (headpart.direction == DIRECTION.WEST && dir == DIRECTION.EAST) ||
                (headpart.direction == DIRECTION.SOUTH && dir == DIRECTION.NORTH)
        ) return;

        for (int i = 0; i < snakeParts.size; i++) {

             int n = snakeParts.size - i - 1;
            Gdx.app.debug(TAG, "PART nmb = " + n);
            //movement from tail
            Part part = snakeParts.get(n);
            DIRECTION newdir = n < 2? dir: snakeParts.get(n - 1).direction;
            partLoading(newdir);
            Gdx.app.debug(TAG, "Move FROM position: x, y = " + part.position.x + ", " + part.position.y);
            //Gdx.app.debug(TAG, "Move FROM render: x, y = " + part.renderPos.x + ", " + part.renderPos.y);

            switch (n ==0? dir: part.direction) { //head move to dir
                case SOUTH:
                    part.position.y = part.position.y - 1;
                    Gdx.app.debug(TAG, "Direction = " + DIRECTION.SOUTH.name());
                    break;
                case WEST:
                    part.position.x = part.position.x - 1;
                    Gdx.app.debug(TAG, "Direction = " + DIRECTION.WEST.name());
                    break;
                case EAST:
                    part.position.x = part.position.x + 1;
                    Gdx.app.debug(TAG, "Direction = " + DIRECTION.EAST.name());
                    break;
                case NORTH:
                    Gdx.app.debug(TAG, "Direction = " + DIRECTION.NORTH.name());
                    part.position.y = part.position.y + 1;
            }
            part.direction = n == 1? dir: newdir; //if neck dir
            //set part's texture
            if (n == 0)
                part.reg = regHead;
            else if (n == snakeParts.size - 1)
                part.reg = regTale;
            else
                part.reg = regWave;
            Gdx.app.debug(TAG, "TO position: x, y = " + part.position.x + ", " + part.position.y);
         }
        //set angle parts
        setAnleParts();
    }

    private void setAnleParts(){
        findConnections();
        for (int i = 1; i < snakeParts.size - 1; i++)
            if (isitConnection(i))
                snakeParts.get(i).reg = regBody;
     }

    private Vector2 positionCorrectin(DIRECTION pre, DIRECTION post){

        Vector2 corrV = new Vector2(0f, 0f);

        if (pre == DIRECTION.NORTH && post == DIRECTION.EAST)
            corrV = corrV.add(0f, 1f);
        if (pre == DIRECTION.NORTH && post == DIRECTION.WEST)
            corrV = corrV.add(1f, 0f);
        if (pre == DIRECTION.EAST && post == DIRECTION.SOUTH)
            corrV = corrV.add(0f, 0f);  //1, 0
        if (pre == DIRECTION.EAST && post == DIRECTION.NORTH)
            corrV = corrV.add(0f, 0f); // 0, -1
        if (pre == DIRECTION.SOUTH && post == DIRECTION.WEST)
            corrV = corrV.add(0f, -1f);
        if (pre == DIRECTION.SOUTH && post == DIRECTION.EAST)
            corrV = corrV.add(-1f, 0f);
        if (pre == DIRECTION.WEST && post == DIRECTION.NORTH)
            corrV = corrV.add(0f, 0f); //-1, 0
        if (pre == DIRECTION.WEST && post == DIRECTION.SOUTH)
            corrV = corrV.add(0f, 1f);

        return corrV;
    }

    public void findConnections(){
        con.clear();
        for (int i = 1; i < snakeParts.size - 1; i++) {
            Part part = snakeParts.get(i);
            Part part1 = snakeParts.get(i + 1);
            if (! (part.direction == part1.direction))
                con.add(new Connection(i, part.direction, part1.direction));
        }
        return;
    }

    private boolean isitConnection(int ipart){
        for (int i = 0; i < con.size; i++) {
            int key = con.get(i).key;
            if (ipart == key) {
                currentKey = i;
                return true;
            }
        }
            return false;
    }

    public void render(SpriteBatch batch) {

        if (dead)
            return;
        //Array <Connection> connection = findConnections();

        TextureRegion reg = null;

        for (int i = 0; i < snakeParts.size; i++){

            //reg = i== 0? regHead : (i == snakeParts.size - 1? regTale: (isitConnection(i, connection)?regBody: regWave));
            Part part = snakeParts.get(i);
            reg = part.reg;
//           Vector2 pos = part.renderPos;
            Vector2 pos = part.position;
            boolean flipx = false;
            boolean flipy = false;
            DIRECTION dtail = DIRECTION.NORTH;
            boolean body = isitConnection(i);
            if(body)
                dtail = snakeParts.get(i + 1).direction;
            switch (part.direction) {
                case NORTH:
                    //flipy = dtail == DIRECTION.EAST;
                    flipx = dtail == DIRECTION.EAST;
                    break;
                case EAST:
                    flipx = i == snakeParts.size - 1 || i == 0;
                    flipy = body && (dtail == DIRECTION.NORTH);
                    break;
                case WEST:
                    flipy = body && (dtail == DIRECTION.NORTH );
                    flipx = body && con.get(currentKey).from == DIRECTION.WEST;
                    break;
                case SOUTH:
                    if(body && dtail == DIRECTION.EAST) { //
                        flipx = true;
                        flipy = true;
                    }
                    else {
                        flipy = true;
                        //flipx = body && dtail == DIRECTION.WEST;
                    }
            }

            batch.draw(reg.getTexture(),
                pos.x, pos.y,
                origin.x,origin.y,
                dimension.x,dimension.y,
                scale.x, scale.y,
                rotation,
                reg.getRegionX(),reg.getRegionY(),
                reg.getRegionWidth(),reg.getRegionHeight(),
                flipx, flipy);
        }
    }
    public void frameFinished(){
        rage = rage - 1 < 0? 0: rage - 1;
    }

    public void killSnake(){
        dead = true;
        mWorldController.level.selectedSnake = (mWorldController.level.selectedSnake + 1) % mWorldController.level.snakeQuantity;
        mWorldController.level.snakeQuantity = mWorldController.level.snakeQuantity - 1;
    }

    public void furyEaten(){
        rage = Constants.FURI_LENGTH;
    }

    public void deleteParts(int num){
        for (int i = 0; i < num; i++)
            deletePart();

    }

    public void deletePart(){
        partLoading(snakeParts.get(snakeParts.size - 2).direction);
        snakeParts.get(snakeParts.size -2).reg = snakeParts.get(snakeParts.size - 1).reg;
         snakeParts.removeIndex(snakeParts.size - 1);
         snakeParts.get(snakeParts.size - 1).reg = regTale;
         setAnleParts();
    }

    public void addPart(DIRECTION dir){

        snakeParts.add(snakeParts.get(snakeParts.size - 1)); //copy tail
        //snakeParts.set(0, head);
        //if(snakeParts.size > 3)
            for(int i = 0; i <  snakeParts.size - 1; i++) {
                int n = snakeParts.size - i - 1;
                snakeParts.set(n, snakeParts.get(n - 1));
            }
        //snakeParts.set(1, part);

        Part head = snakeParts.get(0);
        //add && move
        //DIRECTION dir = head.direction;

        float x = head.position.x;
        float y = head.position.y;

        Vector2 pos = new Vector2(x , y);
        Part part = new Part(dir, pos, regHead);

        if(dir == DIRECTION.NORTH)
            part.position.y = part.position.y + 1;
        else if (dir == DIRECTION.EAST)
            part.position.x = part.position.x + 1;
        else if (dir == DIRECTION.SOUTH)
            part.position.y = part.position.y - 1;
        else if (dir == DIRECTION.WEST)
            part.position.x = part.position.x - 1;
        //part.direction = dir;
        snakeParts.set(0, part);
        snakeParts.get(1).direction = dir;

        setAnleParts();
        flagApple = false;
        for (int i = 0; i < snakeParts.size; i++){
            Part part1 = snakeParts.get(i);
            partLoading(part1.direction);
            if(i == 0)
               part1.reg = regHead;
            else if (i == snakeParts.size - 1)
               part1.reg = regTale;
            else if (isitConnection(i))
               part1.reg = regBody;
            else
               part1.reg = regWave;

        }
    }
}

