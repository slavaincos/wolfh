package com.snakebattle.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class Apple extends AbstractGameObject{
    private TextureRegion regApple;
    public boolean collected;
    public Apple () {
        init();
    }
    private void init () {
        dimension.set(0.5f, 0.5f);
        regApple = Assets.instance.apple.apple;
// Set bounding box for collision detection
        bounds.set(0, 0, dimension.x, dimension.y);
        collected = false;
    }

    public void render (SpriteBatch batch) {
        if (collected) return;
        TextureRegion reg = null;
        reg = regApple;
        batch.draw(reg.getTexture(),
                position.x + 0.25f, position.y + 0.25f,
                origin.x, origin.y,
                dimension.x, dimension.y,
                scale.x, scale.y,
                rotation,
                reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(),
                false, false);
    }
}
