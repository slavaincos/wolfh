package com.snakebattle.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Level {
    public static final String TAG = Level.class.getName();
    public int selectedSnake;
    public int snakeQuantity;


    public enum BLOCK_TYPE {
        EMPTY(0, 0, 0), // black
        STONE(96, 125, 139), // gray
        PLAYER_SPAWNPOINT(255, 255, 255), // white
        APPLE(0, 255, 0), // green
        BORDER(238, 64, 53), // redy
        ITEM_GOLD_COIN(255, 235, 59), // yellow
        FURY(255, 0, 0); //red

        private int color;

        private BLOCK_TYPE(int r, int g, int b) {
            color = r << 24 | g << 16 | b << 8 | 0xff;
        }

        public boolean sameColor(int color) {
            return this.color == color;
        }

        public int getColor() {
            return color;
        }

    }

    // objects
    public Array<Field> field;
    public Array<GoldCoin> goldcoins;
    public Array<Stone> stones;
    public Array<Apple> apples;
    public Array<Fury> furies;
    public Array<Snake> snakes;

    public Level(String filename, WorldController wc) {
        init(filename, wc);
    }

    private void init(String filename, WorldController wc) {

        // objects
        field = new Array<Field>();
        goldcoins = new Array<GoldCoin>();
        stones = new Array<Stone>();
        apples = new Array<Apple>();
        furies = new Array<Fury>();
        snakes = new Array<Snake>();
        selectedSnake = 0;
        snakeQuantity = 2;

// load image file that represents the level data
        Pixmap pixmap = new Pixmap(Gdx.files.internal(filename));
// scan pixels from top-left to bottom-right
        int lastPixel = -1;
        for (int pixelY = 0; pixelY < pixmap.getHeight(); pixelY++) {
            for (int pixelX = 0; pixelX < pixmap.getWidth(); pixelX++) {
                AbstractGameObject obj = null;
                float offsetHeight = 0;
// height grows from bottom to top
                float baseHeight = pixmap.getHeight();
// get color of current pixel as 32-bit RGBA value
                int currentPixel = pixmap.getPixel(pixelX, pixelY);
// find matching color value to identify block type at (x,y)
// point and create the corresponding game object if there is
// a match
// empty space
                if (BLOCK_TYPE.EMPTY.sameColor(currentPixel)) {
// do nothing
                }
// field
                else if (BLOCK_TYPE.BORDER.sameColor(currentPixel)) {
                    obj = new Field();
                    //float heightIncreaseFactor = 0.25f;
                    //offsetHeight = -2.5f;
                    obj.position.set(pixelX + 1,
                            baseHeight - pixelY);
                    field.add((Field) obj);
                }
// player snakes
                else if
                (BLOCK_TYPE.PLAYER_SPAWNPOINT.sameColor(currentPixel)) {
                    Array<Vector2> parts = new Array<Vector2>();
                    parts.add(new Vector2(pixelX + 1, baseHeight - pixelY));
                    parts.add(new Vector2(pixelX + 1, baseHeight - pixelY - 1));
                    obj = new Snake(parts, wc);
                    obj.dimension = new Vector2(1f, 1f);
                    obj.position.set(pixelX + 1,
                            baseHeight - pixelY);
                    snakes.add((Snake) obj);                 }
// stone
                else if (BLOCK_TYPE.STONE.sameColor(currentPixel)) {
                    obj = new Stone();
                    obj.position.set(pixelX + 1,
                            baseHeight - pixelY);
                    stones.add((Stone) obj);
                 }
// gold coin
                else if (BLOCK_TYPE.ITEM_GOLD_COIN.sameColor(currentPixel)) {
                    obj = new GoldCoin();
                    obj.position.set(pixelX + 1,
                            baseHeight - pixelY);
                    goldcoins.add((GoldCoin) obj);
                }
// apple
                else if (BLOCK_TYPE.APPLE.sameColor(currentPixel)) {
                    obj = new Apple();
                    obj.position.set(pixelX + 1,
                            baseHeight - pixelY);
                    apples.add((Apple) obj);
                }
// fury pill
                else if (BLOCK_TYPE.FURY.sameColor(currentPixel)) {
                    obj = new Fury();
                    obj.position.set(pixelX + 1,
                            baseHeight - pixelY);
                    furies.add((Fury) obj);
                }

// unknown object/pixel color
                else {
                    int r = 0xff & (currentPixel >>> 24); //red color channel
                    int g = 0xff & (currentPixel >>> 16); //green color channel
                    int b = 0xff & (currentPixel >>> 8); //blue color channel
                    int a = 0xff & currentPixel; //alpha channel
                    Gdx.app.error(TAG, "Unknown object at x<" + pixelX
                            + "> y<" + pixelY
                            + ">: r<" + r
                            + "> g<" + g
                            + "> b<" + b
                            + "> a<" + a + ">");
                }
                lastPixel = currentPixel;
            }
        }

        AbstractGameObject obj = new Field();
        obj.position.set(0,
                Constants.FILD_SIZE);
        field.add((Field) obj);

        // decoration
// free memory
        pixmap.dispose();
        Gdx.app.debug(TAG, "level '" + filename + "' loaded");
    }

    public void render(SpriteBatch batch) {

 // Draw Field
        field.get(0).render(batch);
 // Draw Gold Coins
        for (GoldCoin goldCoin : goldcoins)
            goldCoin.render(batch);

// Draw stones
        for (Stone stone : stones)
            stone.render(batch);

// Draw apples
        for (Apple apple : apples)
            apple.render(batch);
// Draw apples
        for (Fury fury : furies)
            fury.render(batch);
// Draw snakes
        for (Snake snake : snakes)
            snake.render(batch);
    }
}

