package com.snakebattle.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;

public class Field extends AbstractGameObject {
    private TextureRegion regWhite;
    private TextureRegion regBlack;
    private TextureRegion regSideMiddle;

    public Field(){
        init();
    }

    private void init() {
        dimension.set(1, 1);
        regWhite = Assets.instance.rock.edge;
        regBlack = Assets.instance.rock.middle;
        regSideMiddle = Assets.instance.rock.side;
        bounds.set(0, 0, dimension.x, dimension.y);
/*
        this.position.x = - Gdx.graphics.getWidth()/2;
        this.position.y = -Gdx.graphics.getHeight()/2;
*/
    }

 /*
    public void setLength (int length) {
        this.length = length;
        // Update bounding box for collision detection
        bounds.set(0, 0, dimension.x * length, dimension.y);
    }


    public void increaseLength (int amount) {
        setLength(length + amount);
    }
*/

    @Override
    public void render (SpriteBatch batch) {
        TextureRegion reg = null;
        float relX = 0;
        float relY = 0;
/*

        boolean edges = ((position.x == 1 && (position.y == 1 || position.y == Constants.FILD_SIZE))
                ||(position.x == Constants.FILD_SIZE &&(position.y == 1 || position.y == Constants.FILD_SIZE)));
        reg = edges ?regEdge:( !edges && (position.y != 1 && position.y != Constants.FILD_SIZE)? regSideMiddle:regMiddle);
        boolean flipx = (edges && position.x == Constants.FILD_SIZE) || (reg == regSideMiddle && !(position.y == 1) && !(position.y == Constants.FILD_SIZE) && position.x != Constants.FILD_SIZE);
        boolean flipy = (position.y == 1) || ((reg == regSideMiddle) && (position.x == Constants.FILD_SIZE));

            batch.draw(reg.getTexture(),
                    position.x, position.y,
                    origin.x, origin.y,
                    dimension.x, dimension.y,
                    scale.x, scale.y,
                    rotation,
                    reg.getRegionX(), reg.getRegionY(),
                    reg.getRegionWidth(), reg.getRegionHeight(),
                    flipx, flipy);
*/
        for (int i = 0; i < Constants.FILD_SIZE; i++)
            for(int j = 0; j < Constants.FILD_SIZE; j++)
                if((i + j) % 2 == 1)
                    batch.draw(regBlack, i + 1, j + 1);
                else
                    batch.draw(regWhite, i + 1, j + 1);
     }
}

