package com.snakebattle.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

public class WorldController{
    private static final String TAG =
            WorldController.class.getName();
    public Level level;
    private Float interval;
    // Rectangles for collision detection
    private Rectangle r1 = new Rectangle();
    private Rectangle r2 = new Rectangle();


    private void onCollisionSnakeWithStone(Snake snake, Stone stone) {
        Gdx.app.log(TAG, "Stone collected");
        stone.collected = true;
         snake.score = snake.score + 3;
        if (!snake.isRaged())
        if (snake.snakeParts.size > 4)
            snake.deleteParts(3);
        else {
            snake.killSnake();
        }
    };

     private void onCollisionSnakeWithGoldCoin(Snake snake, GoldCoin goldcoin) {
        Gdx.app.log(TAG, "Gold coin collected");
        goldcoin.collected = true;
        snake.score = snake.score + 10;
    };
    private void onCollisionSnakeWithFury(Snake snake, Fury fury) {
        Gdx.app.log(TAG, "Fury collected");
        snake.furyEaten();
        fury.collected = true;

    };
    private void onCollisionSnakeWithField(Snake snake, Field field) {
        //snake perished
        Gdx.app.log(TAG, "Snake perished");
        snake.killSnake();
    };

    private void onCollisionSnakeWithSnake(Snake activesnake, Snake snake, int partNum) {
        //collision analise
         if(activesnake == snake){   //with herself
             activesnake.deleteParts(activesnake.snakeParts.size - partNum);
//             if(activesnake.snakeParts.size > 3)
//                activesnake.deleteParts(activesnake.snakeParts.size - partNum);
            return;
        }
        Gdx.app.log(TAG, "Snake collision");
        if(partNum < 2) //with another's head
            if (activesnake.isRaged() && !snake.isRaged() || (activesnake.isRaged() && snake.isRaged()))
                snake.killSnake();
            else if (!activesnake.isRaged() && snake.isRaged())
                activesnake.killSnake();
            else {
                int delta = activesnake.snakeParts.size - snake.snakeParts.size;
                if (delta > 2) {
                    snake.killSnake();
                    activesnake.deleteParts(snake.snakeParts.size);
                } else if (delta < -2) {
                    snake.deleteParts(activesnake.snakeParts.size);
                    activesnake.killSnake();
                } else {
                    snake.killSnake();
                    activesnake.killSnake();
                }
            }
            else //with another's body
                if (activesnake.isRaged()) {
                    snake.deleteParts(snake.snakeParts.size - partNum);
                }
                else
                    activesnake.killSnake();
    };

    private void testCollisions () {
        for (int is = 0; is < level.snakes.size; is++) {
            Snake snake = level.snakes.get(is);
            if (snake.dead)
                continue;
            Snake.Part part1 = snake.snakeParts.get(0);
            r1.set(part1.position.x,
                    part1.position.y,
                    part1.bounds.width,
                    part1.bounds.height);
// Test collision: Snake <-> Stone
            for (Stone stone : level.stones) {
                if (stone.collected)
                    continue;
                r2.set(stone.position.x, stone.position.y,
                        stone.bounds.width, stone.bounds.height);
                if (!r1.overlaps(r2))
                    continue;
                onCollisionSnakeWithStone(snake, stone);
// IMPORTANT: must do all collisions for valid
// edge testing on stones.
            }
// Test collision: Snake <-> Gold Coins
            for (GoldCoin goldcoin : level.goldcoins) {
                if (goldcoin.collected)
                    continue;
                r2.set(goldcoin.position.x, goldcoin.position.y,
                        goldcoin.bounds.width, goldcoin.bounds.height);
                if (!r1.overlaps(r2))
                    continue;
                onCollisionSnakeWithGoldCoin(snake, goldcoin);
                break;
            }
// Test collision: Snake <-> Furies
            for (Fury fury : level.furies) {
                if (fury.collected)
                    continue;
                r2.set(fury.position.x, fury.position.y,
                        fury.bounds.width, fury.bounds.height);
                if (!r1.overlaps(r2))
                    continue;
                onCollisionSnakeWithFury(snake, fury);
                break;
            }
// Test collision: Snake <-> Field
            for (Field field : level.field) {
                r2.set(field.position.x, field.position.y,
                        field.bounds.width, field.bounds.height);
                if (!r1.overlaps(r2))
                    continue;
                onCollisionSnakeWithField(snake, field);
                break;
            }
// Test collision: Snake <-> Snakes, parts checking
            for (Snake snake1 : level.snakes) {
                if (snake1.dead)
                    continue;
                for (int i = 0; i < snake1.snakeParts.size; i++) {
                    Snake.Part part = snake1.snakeParts.get(i);
                    r2.set(part.position.x, part.position.y,
                            part.bounds.width, part.bounds.height);
                    if (!r1.overlaps(r2))
                        continue;
                    if(snake == snake1 && i == 0)
                        continue;
                    onCollisionSnakeWithSnake(snake, snake1, i);
                    break;
                }
            }
        }
    }

    public void appleCollision(Rectangle r1, Snake snake){
        // Test collision: Snake <-> Apples
        for (Apple apple : level.apples) {
            if (apple.collected)
                continue;
            r2.set(apple.position.x, apple.position.y,
                    apple.bounds.width, apple.bounds.height);
            if (!r1.overlaps(r2))
                continue;
            //onCollisionSnakeWithApple(snake, apple);
            snake.flagApple = true;
            apple.collected = true;
            snake.score = snake.score + 1;
            break;
        }
    }

    public WorldController() {
        init();
    }

    private void init() {
        //initTestObjects();
        initLevel();
        interval = 0f;
        Gdx.input.setInputProcessor(new InputAdapter(){
            public boolean touchDown(int x, int y, int pointer, int button){
                int ix = x /60 - 1;
                int iy = 8 - y/60;
                Gdx.app.log("touchDown", " x = " + ix + " y = " +  iy);
                Apple rabbit = level.apples.get(0);
                if (MiniMax.instance.canMoveToPosition(rabbit, new Vector2(ix, iy))) {
                    rabbit.position.x = ix;
                    rabbit.position.y = iy;
                    MiniMax.instance.runMinMax(level.goldcoins.get(0), 0, -Constants.EPIC_BIG_VALUE, Constants.EPIC_BIG_VALUE);
                }
                return true;
            }
        });
    }

    private void initLevel(){
        level = new Level(Constants.LEVEL_01, this);
        MiniMax.instance.level = level;
    }

    public void update(float deltaTime) {
        handleDebugInput(deltaTime);
        testCollisions();
     }

    private boolean FrameFinished(float deltaTime) {
        //int fps = Gdx.graphics.getFramesPerSecond();
        interval = interval + deltaTime;
        if (interval > 1f) {
            interval = 0f;
            for(Snake snake: level.snakes)
                snake.frameFinished();
            return true;
        }
        return false;
    }


    private void handleDebugInput (float deltaTime) {
        if (Gdx.app.getType() != Application.ApplicationType.Desktop)
            return;

        if (Gdx.input.isKeyPressed(Input.Keys.SPACE))
            if (FrameFinished(deltaTime)) {
                level.selectedSnake = (level.selectedSnake + 1) % level.snakeQuantity;
                Gdx.app.debug(TAG,"Snag #" + level.selectedSnake + " choosen");
            }
        if (Gdx.input.isKeyPressed(Input.Keys.A))
            if (FrameFinished(deltaTime))
                level.snakes.get(level.selectedSnake).moveTo(Snake.DIRECTION.WEST);
        if (Gdx.input.isKeyPressed(Input.Keys.D))
            if (FrameFinished(deltaTime))
               level.snakes.get(level.selectedSnake).moveTo(Snake.DIRECTION.EAST);
        if (Gdx.input.isKeyPressed(Input.Keys.W))
            if (FrameFinished(deltaTime))
                level.snakes.get(level.selectedSnake).moveTo(Snake.DIRECTION.NORTH);
        if (Gdx.input.isKeyPressed(Input.Keys.X))
            if (FrameFinished(deltaTime))
                level.snakes.get(level.selectedSnake).moveTo(Snake.DIRECTION.SOUTH);

        }

    }





