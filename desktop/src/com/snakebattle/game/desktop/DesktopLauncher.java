package com.snakebattle.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.snakebattle.game.SnakeGame;

import java.io.File;

public class DesktopLauncher {
	private static boolean rebuildAtlas = true;
	private static boolean drawDebugOutline = false;

	public static void main (String[] arg) {
		File input = new File("desktop/assets-raw/images");
		File output = new File("android/assets/images");
		boolean bin = input.exists();
		boolean bout = output.exists();

		if (rebuildAtlas) {
			TexturePacker.Settings settings = new TexturePacker.Settings();
			settings.maxWidth = 1024;
			settings.maxHeight = 1024;
			settings.debug = drawDebugOutline;
			TexturePacker.process(settings, "desktop/assets-raw/images", "android/assets/images",
					"snakefield.atlas");
		}
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "SnakeBattle";
		config.width = 600;
		config.height = 600;
		new LwjglApplication(new SnakeGame(), config);
	}
}
